<?php

namespace Boitebeet\NovaCompareField;

use Closure;
use Laravel\Nova\Fields\Field;

class Compare extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'nova-compare-field';

    public $oldValue;
    public $oldAttribute;

    /**
     * Create a new field.
     *
     * @param  string  $name
     * @param  string|callable|null  $attribute
     * @param  callable|null  $resolveCallback
     * @return void
     */
    public function __construct($name, $newAttribute, $oldAttribute, callable $resolveCallback = null)
    {
        $this->name = $name;
        $this->resolveCallback = $resolveCallback;

        $this->onlyOnDetail();
        $this->default(null);

        if ($newAttribute instanceof Closure ||
            (is_callable($newAttribute) && is_object($newAttribute))) {
            $this->computedCallback = $newAttribute;
            $this->attribute = 'ComputedField';
        } else {
            $this->attribute = $newAttribute;
            $this->oldAttribute = $oldAttribute;
        }
    }

    public function resolve($resource, $attribute = null)
    {
        $this->resource = $resource;

        $attribute = $attribute ?? $this->attribute;

        if (! $this->resolveCallback) {
            $this->value = $this->resolveAttribute($resource, $attribute);
            $this->oldValue = $this->getOldValue($resource);
        } elseif (is_callable($this->resolveCallback)) {
            tap($this->resolveAttribute($resource, $attribute), function ($value) use ($resource, $attribute) {
                $this->value = call_user_func($this->resolveCallback, $value, $resource, $attribute);
            });
            tap($this->getOldValue($resource), function ($value) use ($resource) {
                $this->oldValue = call_user_func($this->resolveCallback, $value, $resource, $this->oldAttribute);
            });
        }
    }

    public function getOldValue($resource)
    {
        return $this->oldValue ?? $this->resolveAttribute($resource, $this->oldAttribute);
    }

    public function oldValue($value)
    {
        $this->oldValue = $value;
    }

    public function newValue($value)
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(),
            [
                'oldValue' => $this->oldValue
            ]);
    }
}
